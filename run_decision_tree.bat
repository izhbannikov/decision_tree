@echo off
echo Starting Decision Tree algorithm...
echo test file: tennis.csv
C:\Python27\python.exe decision_tree.py tennis.csv -c buckets.txt
echo test file: buyer.csv
C:\Python27\python.exe decision_tree.py buyer.csv
echo test file: restaurant.txt
C:\Python27\python.exe decision_tree.py restaurant.txt
echo Done!
pause